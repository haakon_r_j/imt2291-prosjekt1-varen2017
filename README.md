# Oppstart av prosjekt #

Når dere har prosjektet tilgjengelig via bitbucket skal denne readme filen redigeres slik at navnet på alle deltakerne fremgår først i teksten.

Husk å gi meg (oeivind.kolloen@ntnu.no) leserettigheter til repositoriet.

Husk å gjøre jevnlige commits og å bruke Wikien til å dokumentere prosjektet.