<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $pageTitle; ?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
<link rel="stylesheet" href="BootstrapTreeNav/dist/css/bootstrap-treenav.min.css"/>
<link rel="stylesheet" href="css/bookmarks.css"/>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Videolista</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">


        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Innstillinger <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php $s = $user->getAdmin(); //checks if the user is a admin
            if ($s  == 1):  ?>
            <li><a href="addUser.php?uid=<?php echo $user->uid; ?>">Add User</a></li>
            <li><a href="findUser.php?uid=<?php echo $user->uid; ?>">Delete user</a></li>
            <?php endif; ?>
            <li class="divider"></li>
            <li><a href="upload.php">Upload video</a></li>
            <li><a href="finnVideo.php">Delete video</a></li>
            <li class="divider"></li>

          </ul>

        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
		<li>
			<form class="navbar-form navbar-left" role="search" method="post">
	        <div class="form-group">
	          <input type="text" class="form-control" placeholder="Search" name="name">
	        </div>
	         <button type="submit" class="btn btn-default">Submit</button>
	 	    </form>
		</li>

      	<?php
      		if ($user->isLoggedIn())
      			echo '<a href="index.php?logout=true" class="btn btn-default navbar-btn">Sign out</a>';
      		else
      			echo '<a href="signin.php" class="btn btn-default navbar-btn">Sign in</a>'
      	?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-xs-12 col-md-12">
        <?php
          if (!empty($_POST['name'])){
            require_once 'include/db.php';
            /* Henter ut info fra databasen med PDO */
            $stmt = $db->prepare("SELECT name, id, filtype FROM film WHERE name LIKE :name");
            $stmt->bindValue(':name', '%'.$_POST['name'].'%', PDO::PARAM_STR);
            $stmt->execute();

            $row = $stmt->fetch();
            do {
              if($row) {
                echo "<br>";
                /* Sender med video-info som en href til playFile.php */
                echo "<a href='playFile.php?v=".$row['id']."&f=".$row['filtype']."''>".$row["name"]."</a>";
              }
              else echo "Finner ingen resultater";
          } while($row = $stmt->fetch());
        }?>
			</div>
		</div>
  </body>
