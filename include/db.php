<?php
try {	// Attempt a connection to the database
	$db = new PDO('mysql:host=localhost;dbname=imt2291-prosjekt1-varen2017;charset=utf8',
								'root', 'abc123');
} catch (PDOException $e) {	// If an error is detected
	if (isset($debug))		// If we are doing development
		die ('Unable to connect to database : '.$e->getMessage());
	else 					// Do NOT show above information to end users.
		die ('Unable to connect to database, please try again later');
}
