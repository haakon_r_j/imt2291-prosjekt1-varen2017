
        <div class="panel-body" >
          <form id="signupform" class="form-horizontal" role="form" method="post" action="addUser.php">

							<?php echo $alert; ?>

              <div class="form-group">
                  <label for="email" class="col-md-3 control-label">Email</label>
                  <div class="col-md-9">
                      <input type="email" class="form-control" name="email" <?php echo $email; ?> placeholder="Email Address">
                  </div>
              </div>

              <div class="form-group">
                  <label for="firstName" class="col-md-3 control-label">First Name</label>
                  <div class="col-md-9">
                      <input type="text" class="form-control" name="firstName" <?php echo $firstName; ?>placeholder="First Name">
                  </div>
              </div>
              <div class="form-group">
                  <label for="lastName" class="col-md-3 control-label">Last Name</label>
                  <div class="col-md-9">
                      <input type="text" class="form-control" name="lastName" <?php echo $lastName; ?>placeholder="Last Name">
                  </div>
              </div>
              <div class="form-group">
                  <label for="tlf" class="col-md-3 control-label">Telephone</label>
                  <div class="col-md-9">
                      <input type="int" class="form-control" name="tlf" <?php echo $tlf; ?> placeholder="tlf">
                  </div>
              </div>

              <div class="form-group">
                  <label for="admin" class="col-md-3 control-label">Admin</label>
                  <div class="col-md-9">
                      <input type="tinyint" class="form-control" name="admin"<?php echo $admin; ?> placeholder="Admin">
                  </div>
              </div>

              <div class="form-group">
                  <label for="password" class="col-md-3 control-label">Password</label>
                  <div class="col-md-9">
                      <input type="password" class="form-control" name="password" placeholder="Password">
                  </div>
              </div>

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                              <input type="submit" id="btn-signup" class="btn btn-info" value="Sign Up"/>
                                </div>
                        </div>
              </form>
             </div>
            </div>
         </div>
